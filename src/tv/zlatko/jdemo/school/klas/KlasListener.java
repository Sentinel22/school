package tv.zlatko.jdemo.school.klas;

import tv.zlatko.jdemo.school.Student;

/**
 * Created by zlatko on 16.01.18.
 */
public interface KlasListener {

    void added(Klas k, Student s);
    void removed(Klas k, Student s);
    void renamed(Klas k);

    void indexesChanged(Klas k);

}
