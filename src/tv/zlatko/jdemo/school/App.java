package tv.zlatko.jdemo.school;

import tv.zlatko.jdemo.school.klas.Klas;
import tv.zlatko.jdemo.school.klas.KlasModelController;
import tv.zlatko.jdemo.school.klas.KlasTableModel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Vector;

public class App {

    private static final int GAP = 10;

    public static void main(String[] args) {

        final School school = new School();
        school.fillSampleData();

        // UI
        final JFrame frame = new JFrame("Some title");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 800, 500);
        frame.setLayout(new BorderLayout(GAP, GAP));

        final JPanel panelTop = new JPanel(new BorderLayout(GAP, GAP));
        frame.add(panelTop, BorderLayout.NORTH);
        final KlasTableModel dataModel = new KlasTableModel();
        dataModel.setData(school.getKlasses().iterator().next());
        final JTable table = new JTable(dataModel);
        frame.add(new JScrollPane(table), BorderLayout.CENTER);
        final JPanel panelButtons = new JPanel(new GridLayout(10, 1, GAP, GAP));
        frame.add(panelButtons, BorderLayout.EAST);

        panelTop.setBorder(new EmptyBorder(GAP, 0, 0, 0));
        panelTop.add(new JLabel("Изберете клас:"), BorderLayout.WEST);
        final JComboBox<Klas> comboBox = new JComboBox<>(new DefaultComboBoxModel<Klas>(new Vector<Klas>(school.getKlasses())));
        panelTop.add(comboBox, BorderLayout.CENTER);

        final KlasModelController controller = new KlasModelController(dataModel);

        panelButtons.add(new JButton(new AbstractAction("+") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final String s = JOptionPane.showInputDialog(frame, "Enter student name!");
                if (s == null || s.length() == 0) {
                    // user cancel
                    return;
                }
                controller.addNewStudent(s);
            }
        }));

        panelButtons.add(new JButton(new AbstractAction("-") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int selectedRow = table.getSelectedRow();
                if (selectedRow == -1) {
                    JOptionPane.showMessageDialog(panelButtons, "Please select a student!");
                } else {
                    controller.removeStudent(selectedRow);
                }
            }
        }));

        panelButtons.add(new JButton(new AbstractAction("Rename") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int selectedRow = table.getSelectedRow();
                if (selectedRow == -1) {
                    JOptionPane.showMessageDialog(panelButtons, "Please select a student!");
                } else {
                final String s = JOptionPane.showInputDialog(frame, "Enter new name!");
                    if (s == null || s.length() == 0) {
                        // user cancel
                        return;
                    } else controller.renameStudent(s, selectedRow);
                }
            }
        }));

        frame.setVisible(true);
    }
}

