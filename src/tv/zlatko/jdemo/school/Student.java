package tv.zlatko.jdemo.school;

import java.util.Date;
import java.util.TreeSet;

public class Student implements Comparable<Student> {

    private static int lastNumber = 0;

    public String name;
    private Date dob;

    public Student(final String name, final Date dob) {
        this.name = name;
        this.dob = dob;
    }

    public String getName() {
        return name;
    }

    public Date getDob() {
        return dob;
    }

    @Override
    public int compareTo(final Student to6ko) {
        return this.getName().compareTo(to6ko.getName());
    }
}
