package tv.zlatko.jdemo.Main;

public class ChristmasTree {
    public static void main(String[] args) {
        final int levels = 8;
        for (int current = 0; current < levels; current ++) {
            printLevel(current, levels);
        }
    }

    private static void printLevel(final int levelNumber, final int levelsCount) {
        final int width = 1 + levelsCount * 2;
        for (int lineNumber = 0; lineNumber < levelNumber + 1; lineNumber++) {
            printLine(width, lineNumber * 2 + 1);
        }
    }

    private static void printLine(final int width, final int numberOfStars) {
        for (int column = 0; column < width; column ++) {
            final int distanceFromCenter = Math.abs(width / 2 + 1 - column);
            if (distanceFromCenter <= numberOfStars / 2) {
                System.out.print(" * ");
            } else {
                System.out.print("   ");
            }
        }
        System.out.println();
    }

}
