package tv.zlatko.jdemo.school.klas;

import tv.zlatko.jdemo.school.Student;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class KlasTableModel extends AbstractTableModel implements KlasListener {

    Klas klas;

    private List<Student> displayStudents = new ArrayList<>();

    public KlasTableModel() {
    }

    @Override
    public void added(final Klas k, final Student s) {
        if (k != this.klas) {
            throw new IllegalStateException("wtf!!");
        }

        int firstIndex, lastIndex;
        firstIndex = displayStudents.size();
        displayStudents.add(s);
        lastIndex = firstIndex;
        fireTableRowsInserted(firstIndex, lastIndex);
    }

    @Override
    public void removed(final Klas k, final Student s) {
        final int rowIndex = displayStudents.indexOf(s);
        displayStudents.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    @Override
    public void renamed(final Klas k) {
        fireTableRowsUpdated(0, displayStudents.size());
    }

    @Override
    public void indexesChanged(final Klas k) {
        fireTableChanged(new TableModelEvent(this, 0, getRowCount() - 1, 2));
    }

    public void setData(Klas klas) {
        this.klas = klas;
        displayStudents.addAll(klas.getStudents());
        this.klas.register(this);
    }

    @Override
    public int getRowCount() {
        return klas.getStudents().size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(final int column) {
        switch (column) {
            case 0: return "Име";
            case 1: return "Рожден ден";
            case 2: return "Номер";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        final Student student = getStudentAtRow(rowIndex);
//        final Student student = klas.getStudentByIndex(rowIndex);
        switch (columnIndex) {
            case 0: return student.getName();
            case 1: return student.getDob();
            case 2: return klas.getNumber(student);
        }
        return "не бива да виждаш това";
    }

    public Student getStudentAtRow(int rowIndex) {
        return displayStudents.get(rowIndex);
    }
}
