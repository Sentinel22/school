package tv.zlatko.jdemo.school;

import tv.zlatko.jdemo.school.klas.Klas;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class School {

    private List<Klas> klasses = new LinkedList<>();

    public List<Klas> getKlasses() {
        return Collections.unmodifiableList(klasses);
    }

    public void fillSampleData() {
        final Klas a1 = new Klas("Първи А");
        final Student zlatko = new Student("zlatko", new Date());
        final Student go6ko = new Student("go6ko", new Date(99));
        a1.addStudent(zlatko);
        a1.addStudent(go6ko);
        klasses.add(a1);
        klasses.add(new Klas("Първи Б"));
    }

}
