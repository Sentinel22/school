package tv.zlatko.jdemo.school.klas;

import tv.zlatko.jdemo.school.Student;

import java.util.*;

public class Klas {

    public final String name;
    private Set<Student> students = new TreeSet<>();
    private List<Student> indexedStudents = new ArrayList<>();
    private List<KlasListener> listeners = new LinkedList<>();

    public Klas(final String name) {
        this.name = name;
    }

    public void register(KlasListener l) {
        listeners.add(l);
    }

    public void unregister(KlasListener l) {
        listeners.remove(l);
    }

    public void addStudent(Student s) {
        students.add(s);

        recalculateIndexes();

        for (KlasListener listener : listeners) {
            listener.added(this, s);
        }
    }

    public Integer getNumber(Student s) {
        for (int i = 0; i < indexedStudents.size(); i ++) {
            if (s == indexedStudents.get(i)) {
                return i + 1;
            }
        }
        return null;
    }

    public List<Student> getStudents() {
        return Collections.unmodifiableList(indexedStudents);
    }

    @Override
    public String toString() {
        return name;
    }

    public void deleteStudent(final Student student) {
        students.remove(student);

        recalculateIndexes();

        for (KlasListener l : listeners) {
            l.removed(this, student);
        }
    }

    private void recalculateIndexes() {
        indexedStudents.clear();
        for (Student n : students) {
            indexedStudents.add(n);
        }
    }

    public void renameStudent(String name, Student student) {
            students.remove(student);
            student.name=name;
            students.add(student);


        recalculateIndexes();

        for (KlasListener listener : listeners) {
            listener.renamed(this);
        }
    }
}
